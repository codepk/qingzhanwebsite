package com.qz.cn.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qz.cn.dto.SysArticle;
import com.qz.cn.service.SysArticleService;
import com.qz.cn.utils.ReturnResult;
import com.qz.cn.utils.ReturnResultUtils;
import com.qz.cn.vo.ArticleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 文章控制器
 */

@RestController
@Api(tags = "文章内容")
@Log4j
@RequestMapping(value = "/qz/article")
public class SysArticleController {

    @Autowired
    private SysArticleService sysArticleService;

    @ApiOperation(value = "获取文章集合")
    @PostMapping(value = "/queryarticle")
    public ReturnResult<ArticleVo> queryArticle(@RequestBody Map<String,Object> map) {
        try {

            List<ArticleVo> articleVos = sysArticleService.queryArticle((Integer) map.get("page"), (Integer) map.get("size"));
            System.out.println(articleVos.size());
            return ReturnResultUtils.returnSuccess(articleVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"文章返回失败");
    }



    @ApiOperation(value = "获取文章集合,首页展示数据，不包括（9专家观点）")
    @PostMapping(value = "/queryarticles")
    public ReturnResult<ArticleVo> queryArticles(@RequestBody Map<String,Object> map) {
        try {
            List<ArticleVo> articleVos = sysArticleService.queryArticles((Integer) map.get("page"), (Integer) map.get("size"));
            return ReturnResultUtils.returnSuccess(articleVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"文章返回失败");
    }


    @ApiOperation(value = "根据id获取文章的信息")
    @PostMapping(value = "/getone/{articleId}")
    public  ReturnResult<ArticleVo> getOneByArticleId(@PathVariable Integer articleId){

        try {
            ArticleVo articleVos = sysArticleService.queryArticleByArtcileId(articleId);
            return ReturnResultUtils.returnSuccess(articleVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"文章信息查询失败");
    }


    @ApiOperation(value = "获取轮播图片")
    @PostMapping(value = "/queryTitileImg")
    public  ReturnResult<ArticleVo> queryTitleImg(){

        try {
            List<ArticleVo> articleVos = sysArticleService.queryTitleImg();
            return ReturnResultUtils.returnSuccess(articleVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"轮播查询失败");
    }



    @ApiOperation(value = "获取单个")
    @PostMapping(value = "/queryArtcileByMenuId/{id}")
    public ReturnResult<ArticleVo> queryArtcileByMenuId(@PathVariable Integer id, @RequestBody  Map<String,Object> map){
        System.out.println(map.get("page")+"------");
        try {
            List<ArticleVo> articleVos = sysArticleService.queryArticleByMenuId(id,(Integer) map.get("page"),(Integer) map.get("size"));
            return ReturnResultUtils.returnSuccess(articleVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"单个数据查询失败");
    }






}
