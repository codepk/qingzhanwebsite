package com.qz.cn.controller;

import com.qz.cn.param.VistitorParam;
import com.qz.cn.service.SysVisitorService;
import com.qz.cn.utils.ReturnResult;
import com.qz.cn.utils.ReturnResultUtils;
import com.qz.cn.vo.VisitorVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author pankai
 * @create 2020-11-27 11:41
 */
@RestController
@Api(tags = "用户内容")
@Log4j
@RequestMapping(value = "/qz/visitor")
public class SysVisitorController {

    @Autowired
    private SysVisitorService sysVisitorService;


    @ApiOperation(value = "用户注册")
    @PostMapping(value = "/register")
    public ReturnResult registerUser(@RequestBody @ApiParam(value = "注册用户实体类")Map<String,Object> map){
        VistitorParam vistitorParam=new VistitorParam();
        vistitorParam.setVisitorName((String) map.get("visitorName"));
        vistitorParam.setVisitorPassword((String) map.get("visitorPassword"));
        vistitorParam.setVisitorEmail((String) map.get("visitorEmail"));
        System.out.println(vistitorParam.getVisitorName());
        boolean flag = sysVisitorService.addVisitor(vistitorParam);
        try {
            if (flag) {
                return ReturnResultUtils.returnSuccess(666);
            }
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777, "添加失败,用户已存在或添加失败");
    }


    @ApiOperation(value = "用户手机号登录")
    @PostMapping(value = "/loginByPhone")
    public ReturnResult<VisitorVo> loginByPhone(@RequestBody Map<String,Object> map
                                 ){
        try {
            VisitorVo visitorVo = sysVisitorService.loginByPhone((String) map.get("userName"),(String) map.get("password") );
            if (visitorVo != null){
                return ReturnResultUtils.returnSuccess(visitorVo);
            }
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777,"登录失败，用户名或密码错误");
    }


    @ApiOperation(value = "用户邮箱登录")
    @PostMapping(value = "/loginByEmail")
    public ReturnResult<VisitorVo> loginByEmail(@RequestBody Map<String,Object> map){
        System.out.println((String)map.get("visitorEmail")+(String)map.get("password"));
        try {
            VisitorVo visitorVo = sysVisitorService.loginByEmail((String) map.get("visitorEmail"),(String)map.get("password") );
            if (visitorVo != null){
                return ReturnResultUtils.returnSuccess(visitorVo);
            }
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777,"登录失败，用户名或密码错误");
    }

}
