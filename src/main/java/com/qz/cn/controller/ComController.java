package com.qz.cn.controller;

import com.qz.cn.service.ComService;
import com.qz.cn.utils.ReturnResult;
import com.qz.cn.utils.ReturnResultUtils;
import com.qz.cn.vo.CompanyDetailVo;
import com.qz.cn.vo.CompanyProfileDetailVo;
import com.qz.cn.vo.CompanyProfileVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author pankai
 * @create 2020-12-25 14:31
 */
@RestController
@Api(tags = "人工智能企业咨询模块测试API")
@Log4j
@RequestMapping(value = "/company")
public class ComController {

    @Autowired
    private ComService comService;

    @ApiOperation(value = "公司简要信息分页查询")
    @PostMapping(value = "/queryCompanyProfileInfo")
    public ReturnResult<CompanyProfileDetailVo> queryCompanyProfileInformation( @RequestBody @ApiParam(value = "分页") Map<String,Object> map
//            @RequestParam(defaultValue = "1")   @ApiParam(value = "当前页(默认为第一页)") Integer pageNo,
//                                                                               @RequestParam(defaultValue = "10") @ApiParam(value = "页面容量(默认为10条)") Integer pageSize
    ) {

        try {
            System.out.println((Integer) map.get("pageNo")+(Integer) map.get("pageSize"));
            CompanyProfileDetailVo companyProfileDetailVo = comService.queryCompanyProfileInformation((Integer) map.get("pageNo"),(Integer) map.get("pageSize"));
            return ReturnResultUtils.returnSuccess(companyProfileDetailVo);
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777, "出大问题了，公司简要信息分页查询失败!");
    }


    @ApiOperation(value = "搜索框搜索公司")
    @PostMapping(value = "/queryCompanyProfileByCN")
    public ReturnResult<CompanyProfileDetailVo> queryCompanyProfileByCompanyName(@RequestBody @ApiParam(value = "公司名称") Map<String,Object> map) {
        try {
            System.out.println((String) map.get("companyName"));
            CompanyProfileDetailVo companyProfileDetailVo = comService.queryCompanyProfileByCompanyName((Integer) map.get("pageNo"), (Integer) map.get("pageSize"), (String) map.get("companyName"));
            return ReturnResultUtils.returnSuccess(companyProfileDetailVo);
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777,"出大问题了，搜索公司简要信息分页查询失败!");

    }

    @ApiOperation(value = "查看公司详情页")
    @PostMapping(value = "/queryCompanyByDetail/{comId}")
    public ReturnResult<CompanyDetailVo> queryCompanyByDetail(@PathVariable @ApiParam(value = "公司的comId") Integer comId){

        try {
            System.out.println(comId);
            CompanyDetailVo companyDetailVo = comService.queryCompanyByDetail(comId);
            return ReturnResultUtils.returnSuccess(companyDetailVo);
        } catch (Exception e) {
            log.error(e);
        }
        return ReturnResultUtils.returnFail(777,"出大问题了，返回公司详情失败!!");
    }

}
