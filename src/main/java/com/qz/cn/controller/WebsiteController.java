package com.qz.cn.controller;

import com.qz.cn.service.WebsiteService;
import com.qz.cn.utils.ReturnResult;
import com.qz.cn.utils.ReturnResultUtils;
import com.qz.cn.vo.ArticleVo;
import com.qz.cn.vo.SecondVo;
import com.qz.cn.vo.TypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author pankai
 * @create 2020-11-10 12:57
 */
@RestController
@Api(tags = "清湛人智院官网")
@Log4j
@RequestMapping(value = "/qz")
public class WebsiteController {

    @Autowired
    private WebsiteService websiteService;

    @ApiOperation(value = "导航栏元素获取")
    @PostMapping(value = "/queryType")
    public ReturnResult<TypeVo> queryType() {

        try {
            List<TypeVo> typeVos = websiteService.queryType();
            return ReturnResultUtils.returnSuccess(typeVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"导航栏返回失败");
    }

    @ApiOperation(value = "二级菜单元素获取")
    @PostMapping(value = "/queryMenu")
    public ReturnResult<SecondVo> queryMenu(@RequestParam @ApiParam(value = "对应导航栏的id,参数2-5即可",required = true) Integer typeId) {

        try {
            List<SecondVo> secondVos = websiteService.queryMenu(typeId);
            return ReturnResultUtils.returnSuccess(secondVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"二级菜单返回失败");
    }

    @PostMapping(value = "/queryArticle")
    public ReturnResult<ArticleVo> queryArticle(){
        List<ArticleVo> articleVos = websiteService.queryArticle();
        return ReturnResultUtils.returnSuccess(articleVos);
    }

}
