package com.qz.cn.controller;

import com.qz.cn.service.SysProjectService;
import com.qz.cn.utils.ReturnResult;
import com.qz.cn.utils.ReturnResultUtils;
import com.qz.cn.vo.ArticleVo;
import com.qz.cn.vo.ProjectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping(value = "/qz/project")
@Api(tags = "融资项目")
@Log4j
public class SysProjectController {


    @Autowired
    private SysProjectService sysProjectService;



    @ApiOperation(value = "查询融资项目集合")
    @PostMapping(value = "/queryProject")
    public ReturnResult<ProjectVo> queryProject(@RequestBody  Map<String, Object> map) {
        try {
            List<ProjectVo> projectVos = sysProjectService.queryAll((Integer) map.get("page"),(Integer) map.get("size"));
            return ReturnResultUtils.returnSuccess(projectVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"融资项目返回失败");
    }


    /**
     * 业务处理
     * @param map
     * @return
     */
    @ApiOperation(value = "查询融资项目集合")
    @PostMapping(value = "/queryProjects")
    public ReturnResult<ProjectVo> queryProjects(@RequestBody  Map<String, Object> map) {
        try {
            List<ProjectVo> projectVos = sysProjectService.queryAlls((Integer) map.get("page"),(Integer) map.get("size"));
            return ReturnResultUtils.returnSuccess(projectVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"融资项目返回失败");
    }


    @ApiOperation(value = "模糊查询融资项目集合")
    @PostMapping(value = "/querySearch")
    public ReturnResult<ProjectVo> querySearch(@RequestBody  Map<String, Object> map) {
        try {
            System.out.println(map.get("name"));
            List<ProjectVo> projectVos = sysProjectService.querySearch((Integer) map.get("page"),(Integer) map.get("size"),(String) map.get("name"));
            return ReturnResultUtils.returnSuccess(projectVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"融资项目返回失败");
    }


    @ApiOperation(value = "查询融资项目详情")
    @PostMapping(value = "/queryone/{id}")
    public ReturnResult<ProjectVo> queryOne(@PathVariable Integer id) {

        try {
            ProjectVo projectVos = sysProjectService.queryProjectByProjectId(id);
            return ReturnResultUtils.returnSuccess(projectVos);
        } catch (Exception e) {
            log.error("查询失败，错误：" + e);
        }
        return ReturnResultUtils.returnFail(777,"融资项目返回失败");
    }



}
