package com.qz.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QingzhanwebsiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(QingzhanwebsiteApplication.class, args);
    }

}
