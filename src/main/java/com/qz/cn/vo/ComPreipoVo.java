package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author pankai
 * @create 2020-12-27 14:50
 */
@ApiModel(value = "ipo排队信息")
@Data
public class ComPreipoVo {

    @ApiModelProperty(value = "公司ipo排队事件自增ID")
    private Integer id;

    @ApiModelProperty(value = "排队阶段")
    private String stage;

    @ApiModelProperty(value = "时间")
    private String time;
}
