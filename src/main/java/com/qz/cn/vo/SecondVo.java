package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pankai
 * @create 2020-11-10 15:48
 */
@ApiModel(value = "二级菜单详情")
@Data
public class SecondVo implements Serializable {

    @ApiModelProperty(value = "菜单栏id")
    private Integer secondId;

    @ApiModelProperty(value = "二级菜单名称")
    private String secondName;
}
