package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author pankai
 * @create 2020-12-27 14:19
 */
@ApiModel(value = "公司融资额外信息，根据com_invest表中的invest_id分类")
@Data
public class ComForeignInvestmentVo {

    @ApiModelProperty(value = "投资事件id,可以选择展示invseId最大的")
    private Integer invseId;

    @ApiModelProperty(value = "对外投资公司名称")
    private String invseComName;

    @ApiModelProperty(value = "投资轮次")
    private String invseRoundName;

    @ApiModelProperty(value = "投资金额")
    private String invseMoney;

    @ApiModelProperty(value = "投资币种")
    private String invseCurrencyName;

    @ApiModelProperty(value = "投资时间")
    private Date invseDate;
}
