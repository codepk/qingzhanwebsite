package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 16:01
 */
@ApiModel(value = "里程碑")
@Data
public class ComMilestoneVo {

    @ApiModelProperty(value = "公布时间-年")
    private Integer comMilYear;

    @ApiModelProperty(value = "公布时间-月")
    private Integer comMilMonth;

    @ApiModelProperty(value = "公布信息")
    private String comMilDetail;
}
