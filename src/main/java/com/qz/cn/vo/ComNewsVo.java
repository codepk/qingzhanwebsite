package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 15:53
 */
@ApiModel(value = "新闻动态")
@Data
public class ComNewsVo {

    @ApiModelProperty(value = "新闻发生时间-年")
    private Integer comNewYear;

    @ApiModelProperty(value = "新闻发生时间-月")
    private Integer comNewMonth;

    @ApiModelProperty(value = "新闻发生时间-日")
    private Integer comNewDay;

    @ApiModelProperty(value = "新闻名称")
    private String comNewName;

    @ApiModelProperty(value = "新闻链接")
    private String comNewUrl;

    @ApiModelProperty(value = "新闻分类名称")
    private String comNewTypeName;
}
