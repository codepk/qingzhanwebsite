package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "融资项目")
public class ProjectVo implements Serializable {

    @ApiModelProperty(value = "项目Id")
    private Integer projectId;

    @ApiModelProperty(value = "所属行业")
    private String SecondId;
    @ApiModelProperty(value = "公司官网")
    private String  companyWebsite;
    @ApiModelProperty(value = "项目名称")
    private String projectName;
    @ApiModelProperty(value = "行业标签")
    private String industryName;
    @ApiModelProperty(value = "轮次")
    private String projectRound;
    @ApiModelProperty(value = "项目估值")
    private String projectValuation;
    @ApiModelProperty(value = "公司简介")
    private String companyIntro;
    @ApiModelProperty(value = "公司logo")
    private String companyLogo;
    @ApiModelProperty(value = "发布时间")
    private String releaseTime;
    @ApiModelProperty(value = "总数量")
    private Integer total;
}
