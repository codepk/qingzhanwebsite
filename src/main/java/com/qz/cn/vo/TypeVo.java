package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pankai
 * @create 2020-11-10 13:09
 */
@ApiModel(value = "导航栏详情")
@Data
public class TypeVo implements Serializable {

    @ApiModelProperty(value = "导航栏id")
    private Integer typeId;


    @ApiModelProperty(value = "导航栏名称")
    private String typeName;
}
