package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pankai
 * @create 2020-11-16 13:52
 */

@ApiModel(value = "文章详情")
@Data
public class ArticleVo implements Serializable {


    @ApiModelProperty(value = "文章Id")
    private Integer articleId;
    @ApiModelProperty(value = "文章标题")
    private String articleName;
    @ApiModelProperty(value = "作者名字")
    private String authorName;
    //    @ApiModelProperty(value = "文章所属类别")
//    private String Type;
    @ApiModelProperty(value = "文章点击量")
    private Integer articleRank;
    @ApiModelProperty(value = "文章内容")
    private String articleContent;
    @ApiModelProperty(value = "文章摘要")
    private String articleSummary;
    @ApiModelProperty(value = "所属分类")
    private String SecondId;
    /**
     * 标题图片
     */
    @ApiModelProperty(value = "文章图片")
    private String articleImage;
    @ApiModelProperty(value = "发表时间")
    private String createTime;
    @ApiModelProperty(value = "总数量")
    private  Integer total;

}
