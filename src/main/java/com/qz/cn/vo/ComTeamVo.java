package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 15:40
 */
@ApiModel(value = "团队信息")
@Data
public class ComTeamVo {

    @ApiModelProperty(value = "人物头像")
    private String perLogo;

    @ApiModelProperty(value = "人物名称")
    private String perName;

    @ApiModelProperty(value = "人物职位")
    private String des;

    @ApiModelProperty(value = "人物介绍")
    private String introduce;
}
