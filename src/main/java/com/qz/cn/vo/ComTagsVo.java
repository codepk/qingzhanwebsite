package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-26 23:18
 */
@ApiModel(value = "公司标签实体类")
@Data
public class ComTagsVo {

    @ApiModelProperty(value = "公司标签自增ID")
    private Integer id;

    @ApiModelProperty(value = "标签ID")
    private Integer tagId;

    @ApiModelProperty(value = "标签名称")
    private String tagName;
}
