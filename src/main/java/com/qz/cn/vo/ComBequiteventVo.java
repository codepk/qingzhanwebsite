package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author pankai
 * @create 2020-12-27 15:14
 */
@ApiModel(value = "退出资本信息")
@Data
public class ComBequiteventVo {

    @ApiModelProperty(value = "退出事件id")
    private Integer quitId;

    @ApiModelProperty(value = "退出方式名称")
    private String quitModeName;

    @ApiModelProperty(value = "退出时间")
    private String quitTime;

    @ApiModelProperty(value = "退出方名称，可能会有多个，同一退出时间quitId相同")
    private List<ComBequiteventExtraVo> comBequiteventExtraVos;
}
