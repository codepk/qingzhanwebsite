package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 15:47
 */
@ApiModel(value = "产品信息")
@Data
public class ComProductVo {

    @ApiModelProperty(value = "产品名称")
    private String comProName;

    @ApiModelProperty(value = "产品简单介绍")
    private String comProDetail;

}
