package com.qz.cn.vo;

import com.qz.cn.dto.ComTags;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author pankai
 * @create 2020-12-26 16:27
 */
@ApiModel(value = "单个公司的详情信息")
@Data
public class CompanyDetailVo implements Serializable {

    @ApiModelProperty(value = "公司LOGO")
    private String comLogoArchive;

    @ApiModelProperty(value = "公司微博")
    private String comWeiboUrl;

    @ApiModelProperty(value = "公司微信")
    private String comWeixinUrl;

    @ApiModelProperty(value = "公司名称")
    private String comName;

    @ApiModelProperty(value = "公司简称")
    private String comSecName;

    @ApiModelProperty(value = "一句话介绍公司")
    private String comSlogan;

    @ApiModelProperty(value = "公司网址")
    private String comUrl;

    @ApiModelProperty(value = "公司当前融资轮次信息")
    private String comFundStatusName;

    @ApiModelProperty(value = "公司电话")
    private String comContTel;

    @ApiModelProperty(value = "公司邮箱")
    private String comContEmail;

    @ApiModelProperty(value = "公司详细地址")
    private String comContAddr;

    @ApiModelProperty(value = "行业名称")
    private String catName;

    @ApiModelProperty(value = "子行业名称")
    private String subCatName;

    @ApiModelProperty(value = "公司标签(多个)")
    private List<ComTagsVo> comTagsVos;

    @ApiModelProperty(value = "公司描述信息")
    private String comDes;

    @ApiModelProperty(value = "公司注册名称")
    private String comRegisteredName;

    @ApiModelProperty(value = "公司创建年")
    private Integer comBornYear;

    @ApiModelProperty(value = "公司创建月")
    private Integer comBornMonth;

    @ApiModelProperty(value = "公司规模信息")
    private String comScaleName;

    @ApiModelProperty(value = "公司状态信息")
    private String comStatusName;

    @ApiModelProperty(value = "融资信息展示")
    private List<ComInvsetVo> comInvsetVos;

    @ApiModelProperty(value = "对外投资集合，根据ComForeignInvestment表进行查询，自定义展示")
    private List<ComForeignInvestmentVo> comForeignInvestmentVos;

    @ApiModelProperty(value = "收购信息集合")
    private List<ComForeignMergerVo> comForeignMergerVos;

    @ApiModelProperty(value = "IPO排队信息")
    private List<ComPreipoVo> comPreipoVos;

    @ApiModelProperty(value = "退出信息")
    private List<ComBequiteventVo> comBequiteventVos;

    @ApiModelProperty(value = "团队信息")
    private List<ComTeamVo> comTeamVos;

    @ApiModelProperty(value = "产品信息")
    private List<ComProductVo> comProductVos;

    @ApiModelProperty(value = "新闻动态")
    private List<ComNewsVo> comNewsVos;

    @ApiModelProperty(value = "里程碑")
    private List<ComMilestoneVo> comMilestoneVos;
}
