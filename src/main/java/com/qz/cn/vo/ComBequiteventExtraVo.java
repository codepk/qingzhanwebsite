package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 15:28
 */
@ApiModel(value = "退出资本名称")
@Data
public class ComBequiteventExtraVo {

    @ApiModelProperty(value = "退出方名称,根据quitId查询")
    private String quitPartyName;
}
