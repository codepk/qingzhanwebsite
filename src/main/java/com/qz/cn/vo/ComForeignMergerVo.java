package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-28 10:27
 */
@ApiModel(value = "收购信息表")
@Data
public class ComForeignMergerVo {

    @ApiModelProperty(value = "被并购方名称")
    private String comName;

    @ApiModelProperty(value = "并购时间-年")
    private Integer mergerShowYear;

    @ApiModelProperty(value = "并购时间-月")
    private Integer mergerShowMonth;

    @ApiModelProperty(value = "并购时间-日")
    private Integer mergerShowDay;

    @ApiModelProperty(value = "并购模糊金额")
    private String mergerAssessMoneyName;

    @ApiModelProperty(value = "并购币种")
    private String mergerCurrencyName;
}
