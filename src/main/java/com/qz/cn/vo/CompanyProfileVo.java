package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pankai
 * @create 2020-12-25 14:40
 */
@ApiModel(value = "公司简要信息")
@Data
public class CompanyProfileVo implements Serializable {

    @ApiModelProperty(value = "公司ID")
    private Integer comId;
    
    @ApiModelProperty(value = "公司缩略图")
    private String comLogo;

    @ApiModelProperty(value = "公司名称")
    private String comName;

    @ApiModelProperty(value = "一句话介绍公司")
    private String comSlogan;

    @ApiModelProperty(value = "公司位置省份")
    private String comProv;

    @ApiModelProperty(value = "公司创建年")
    private Integer comBornYear;

    @ApiModelProperty(value = "公司创建月")
    private Integer comBornMonth;

    @ApiModelProperty(value = "行业名称")
    private String catName;

    @ApiModelProperty(value = "公司当前融资轮次信息")
    private String comFundStatusName;

    @ApiModelProperty(value = "公司总融资金额")
    private String invseTotalMoney;
}
