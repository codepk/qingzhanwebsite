package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pankai
 * @create 2020-11-27 14:23
 */
@Data
@ApiModel("用户详细信息")
public class VisitorVo implements Serializable {
    @ApiModelProperty("用户ID")
    private Integer visitorId;
    @ApiModelProperty("用户名")
    private String visitorName;
    @ApiModelProperty("用户手机号")
    private String visitorPhone;
    @ApiModelProperty("用户邮箱")
    private String visitorEmail;
}
