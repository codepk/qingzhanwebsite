package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author pankai
 * @create 2020-12-27 13:57
 */
@ApiModel(value = "公司融资额外信息，根据com_invest表中的invest_id分类")
@Data
public class ComInvestExtraVo {

    @ApiModelProperty(value = "投资方名称")
    private String name;

    @ApiModelProperty(value = "投资方类型")
    private String type;
}
