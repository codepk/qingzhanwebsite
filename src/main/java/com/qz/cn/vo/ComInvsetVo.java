package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author pankai
 * @create 2020-12-27 13:41
 */
@ApiModel(value = "公司融资信息")
@Data
public class ComInvsetVo {

    @ApiModelProperty(value = "投资数据ID,展示id最大的")
    private Integer invseId;

    @ApiModelProperty(value = "融资时间-年")
    private Integer invseYear;

    @ApiModelProperty(value = "融资时间-月")
    private Integer invseMonth;

    @ApiModelProperty(value = "融资时间-日")
    private Integer invseDay;

    @ApiModelProperty(value = "融资轮次名称")
    private String invseRoundName;

    @ApiModelProperty(value = "融资近似金额名称")
    private String invseSimilarMoneyName;

    @ApiModelProperty(value = "融资币种名称")
    private String invseCurrencyName;

    @ApiModelProperty(value = "根据invseId，查询单个融资项目的投资方")
    private List<ComInvestExtraVo> comInvestExtraVos;

}
