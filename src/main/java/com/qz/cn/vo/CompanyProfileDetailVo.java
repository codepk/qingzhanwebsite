package com.qz.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author pankai
 * @create 2020-12-25 16:30
 */
@ApiModel(value = "公司简要信息和查询总条数")
@Data
public class CompanyProfileDetailVo implements Serializable {

    @ApiModelProperty(value = "查询公司简要信息")
    private List<CompanyProfileVo> companyProfileVos;
    @ApiModelProperty(value = "查询总条数")
    private Integer totalCount;
}
