package com.qz.cn.service;

import com.qz.cn.vo.ArticleVo;
import com.qz.cn.vo.SecondVo;
import com.qz.cn.vo.TypeVo;

import java.util.List;

/**
 * @author pankai
 * @create 2020-11-10 13:04
 */
public interface WebsiteService {
    /**
     * 查询导航栏
     * @return
     */
    List<TypeVo> queryType();

    /**
     * 查询
     * @param typeId
     * @return
     */
    List<SecondVo> queryMenu(Integer typeId);

    List<ArticleVo> queryArticle();

}
