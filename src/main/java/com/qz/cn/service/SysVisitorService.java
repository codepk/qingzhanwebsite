package com.qz.cn.service;

import com.qz.cn.param.VistitorParam;
import com.qz.cn.vo.VisitorVo;

/**
 * @author pankai
 * @create 2020-11-27 13:41
 */
public interface SysVisitorService {

    /**
     * 添加注册用户
     * @param vistitorParam
     * @return
     */
    boolean addVisitor(VistitorParam vistitorParam);

    VisitorVo loginByPhone(String userName,String password);

    VisitorVo loginByEmail(String visitorEmail,String password);

}
