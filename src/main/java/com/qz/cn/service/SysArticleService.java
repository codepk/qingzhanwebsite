package com.qz.cn.service;


import com.qz.cn.vo.ArticleVo;

import java.util.List;

public interface SysArticleService {


    /**
     * 查询文章
     * @return 文章数据
     */
    List<ArticleVo> queryArticle(Integer page,Integer size);





    /**
     * 查询文章
     * @return 文章数据
     */
    List<ArticleVo> queryArticles(Integer page,Integer size);






    /**
     * 根据文章id获取数据
     */
    ArticleVo queryArticleByArtcileId(Integer id);

    /**
     * 根据分类获取
     * @param id
     * @return
     */
    List<ArticleVo> queryArticleByMenuId(Integer id,Integer page,Integer size);




    /**
     * 获取轮播
     * @return
     */
    List<ArticleVo>    queryTitleImg();
}
