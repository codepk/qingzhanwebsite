package com.qz.cn.service;


import com.qz.cn.dto.SysProject;
import com.qz.cn.vo.ProjectVo;

import java.util.List;

/**
 * 融资项目
 */
public interface SysProjectService {


    /**
     * 查询分页融资项目
     * @return
     */
    List<ProjectVo> queryAll(Integer page, Integer size);


    /**
     * 查询项目需要处理
     * @return
     */
    List<ProjectVo> queryAlls(Integer page, Integer size);



    /**
     * 模糊查询并且分页
     * @param page
     * @param size
     * @param name 模糊条件
     * @return
     */
    List<ProjectVo> querySearch(Integer page, Integer size,  String name);


    /**
     * 获取单个融资项目的数据
     * @param id 文章id
     * @return 实体类
     */
    ProjectVo queryProjectByProjectId(Integer id);
}
