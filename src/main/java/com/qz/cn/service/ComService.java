package com.qz.cn.service;

import com.qz.cn.vo.CompanyDetailVo;
import com.qz.cn.vo.CompanyProfileDetailVo;
import com.qz.cn.vo.CompanyProfileVo;

import java.util.List;

/**
 * @author pankai
 * @create 2020-12-25 14:55
 */
public interface ComService {
    /**
     * 分页展示人工智能公司的简要信息
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    CompanyProfileDetailVo queryCompanyProfileInformation(Integer pageNo, Integer pageSize);

    CompanyProfileDetailVo queryCompanyProfileByCompanyName(Integer pageNo, Integer pageSize, String companyName);

    CompanyDetailVo queryCompanyByDetail(Integer comId);
}
