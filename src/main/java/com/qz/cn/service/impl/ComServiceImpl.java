package com.qz.cn.service.impl;

import com.github.pagehelper.PageHelper;
import com.qz.cn.dto.*;
import com.qz.cn.mapper.*;
import com.qz.cn.service.ComService;
import com.qz.cn.utils.DateUtils;
import com.qz.cn.utils.DivisionUtils;
import com.qz.cn.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author pankai
 * @create 2020-12-25 14:56
 */
@Service
public class ComServiceImpl implements ComService {

    @Autowired
    private ComBaseInfoMapper comBaseInfoMapper;
    @Autowired
    private ComBequiteventMapper comBequiteventMapper;
    @Autowired
    private ComBequiteventExtraMapper comBequiteventExtraMapper;
    @Autowired
    private ComForeignInvestmentMapper comForeignInvestmentMapper;
    @Autowired
    private ComForeignMergerMapper comForeignMergerMapper;
    @Autowired
    private ComInvestMapper comInvestMapper;
    @Autowired
    private ComInvestExtraMapper comInvestExtraMapper;
    @Autowired
    private ComMilestoneMapper comMilestoneMapper;
    @Autowired
    private ComNewsMapper comNewsMapper;
    @Autowired
    private ComPreipoMapper comPreipoMapper;
    @Autowired
    private ComProductMapper comProductMapper;
    @Autowired
    private ComSimilarCompanyMapper comSimilarCompanyMapper;
    @Autowired
    private ComTagsMapper comTagsMapper;
    @Autowired
    private ComTeamMapper comTeamMapper;


    @Override
    public CompanyProfileDetailVo queryCompanyProfileInformation(Integer pageNo, Integer pageSize) {

        CompanyProfileDetailVo companyProfileDetailVo = new CompanyProfileDetailVo();

        ComBaseInfoExample comBaseInfoExample = new ComBaseInfoExample();
        comBaseInfoExample.setOrderByClause("com_id");
        PageHelper.startPage(pageNo, pageSize);
        List<ComBaseInfo> comBaseInfos = comBaseInfoMapper.selectByExample(comBaseInfoExample);
        List<CompanyProfileVo> companyProfileVos = new ArrayList<CompanyProfileVo>();
        comBaseInfos.forEach(comBaseInfo -> {
            CompanyProfileVo companyProfileVo = new CompanyProfileVo();
            BeanUtils.copyProperties(comBaseInfo, companyProfileVo);
            // 将融资金额做处理
            String invseTotalMoney = companyProfileVo.getInvseTotalMoney();
            if (invseTotalMoney.length() > 4) {
                int intInvseTotalMoney = Integer.parseInt(invseTotalMoney);
                String division = DivisionUtils.division(intInvseTotalMoney, 10000);
                companyProfileVo.setInvseTotalMoney(division + "亿元");
            }else {
                companyProfileVo.setInvseTotalMoney(invseTotalMoney+"万元");
            }
            companyProfileVos.add(companyProfileVo);
        });
        long totalCount = comBaseInfoMapper.countByExample(comBaseInfoExample);

        companyProfileDetailVo.setCompanyProfileVos(companyProfileVos);
        companyProfileDetailVo.setTotalCount((int) totalCount);
        return companyProfileDetailVo;
    }

    @Override
    public CompanyProfileDetailVo queryCompanyProfileByCompanyName(Integer pageNo, Integer pageSize, String companyName) {

        CompanyProfileDetailVo companyProfileDetailVo = new CompanyProfileDetailVo();
        ComBaseInfoExample comBaseInfoExample = new ComBaseInfoExample();
        comBaseInfoExample.createCriteria().andComNameLike('%' + companyName + '%');
        comBaseInfoExample.setOrderByClause("com_id");
        PageHelper.startPage(pageNo, pageSize);
        List<ComBaseInfo> comBaseInfos = comBaseInfoMapper.selectByExample(comBaseInfoExample);
        List<CompanyProfileVo> companyProfileVos = new ArrayList<CompanyProfileVo>();
        comBaseInfos.forEach(comBaseInfo -> {
            CompanyProfileVo companyProfileVo = new CompanyProfileVo();
            BeanUtils.copyProperties(comBaseInfo, companyProfileVo);
            companyProfileVos.add(companyProfileVo);
        });
        long totalCount = comBaseInfoMapper.countByExample(comBaseInfoExample);

        companyProfileDetailVo.setCompanyProfileVos(companyProfileVos);
        companyProfileDetailVo.setTotalCount((int) totalCount);
        return companyProfileDetailVo;
    }

    @Override
    public CompanyDetailVo queryCompanyByDetail(Integer comId) {
        CompanyDetailVo companyDetailVo = new CompanyDetailVo();
        ComBaseInfoExample comBaseInfoExample = new ComBaseInfoExample();
        comBaseInfoExample.createCriteria().andComIdEqualTo(comId);
        List<ComBaseInfo> comBaseInfos = comBaseInfoMapper.selectByExample(comBaseInfoExample);
        ComBaseInfo comBaseInfo = comBaseInfos.get(0);
        BeanUtils.copyProperties(comBaseInfo, companyDetailVo);

        // 公司多个标签查询
        List<ComTagsVo> comTagsVos = new ArrayList<ComTagsVo>();
        ComTagsExample comTagsExample = new ComTagsExample();
        comTagsExample.createCriteria().andComIdEqualTo(comId);
        List<ComTags> comTags = comTagsMapper.selectByExample(comTagsExample);
        comTags.forEach(comTag -> {
            ComTagsVo comTagsVo = new ComTagsVo();
            BeanUtils.copyProperties(comTag, comTagsVo);
            comTagsVos.add(comTagsVo);
        });
        companyDetailVo.setComTagsVos(comTagsVos);

        List<ComInvsetVo> comInvsetVos = new ArrayList<ComInvsetVo>();
        ComInvestExample comInvestExample = new ComInvestExample();
        comInvestExample.createCriteria().andComIdEqualTo(comId);
        List<ComInvest> comInvests = comInvestMapper.selectByExample(comInvestExample);
        comInvests.forEach(comInvest -> {
            ComInvsetVo comInvsetVo = new ComInvsetVo();
            BeanUtils.copyProperties(comInvest, comInvsetVo);
            Integer invseId = comInvest.getInvseId();
            ComInvestExtraExample comInvestExtraExample = new ComInvestExtraExample();
            comInvestExtraExample.createCriteria().andInvseIdEqualTo(invseId);
            List<ComInvestExtra> comInvestExtras = comInvestExtraMapper.selectByExample(comInvestExtraExample);
            List<ComInvestExtraVo> comInvestExtraVos = new ArrayList<ComInvestExtraVo>();
            comInvestExtras.forEach(comInvestExtra -> {
                ComInvestExtraVo comInvestExtraVo = new ComInvestExtraVo();
                BeanUtils.copyProperties(comInvestExtra, comInvestExtraVo);
                comInvestExtraVos.add(comInvestExtraVo);
            });
            comInvsetVo.setComInvestExtraVos(comInvestExtraVos);
            comInvsetVos.add(comInvsetVo);
        });
        companyDetailVo.setComInvsetVos(comInvsetVos);


        ComForeignInvestmentExample comForeignInvestmentExample = new ComForeignInvestmentExample();
        comForeignInvestmentExample.createCriteria().andComIdEqualTo(comId);
        List<ComForeignInvestment> comForeignInvestments = comForeignInvestmentMapper.selectByExample(comForeignInvestmentExample);
        List<ComForeignInvestmentVo> comForeignInvestmentVos = new ArrayList<ComForeignInvestmentVo>();
        comForeignInvestments.forEach(comForeignInvestment -> {
            ComForeignInvestmentVo comForeignInvestmentVo = new ComForeignInvestmentVo();
            BeanUtils.copyProperties(comForeignInvestment, comForeignInvestmentVo);
            comForeignInvestmentVos.add(comForeignInvestmentVo);
        });
        companyDetailVo.setComForeignInvestmentVos(comForeignInvestmentVos);

        ComForeignMergerExample comForeignMergerExample = new ComForeignMergerExample();
        comForeignMergerExample.createCriteria().andComIdEqualTo(comId);
        List<ComForeignMerger> comForeignMergers = comForeignMergerMapper.selectByExample(comForeignMergerExample);
        List<ComForeignMergerVo> comForeignMergerVos = new ArrayList<ComForeignMergerVo>();
        comForeignMergers.forEach(comForeignMerger -> {
            ComForeignMergerVo comForeignMergerVo = new ComForeignMergerVo();
            BeanUtils.copyProperties(comForeignMerger, comForeignMergerVo);
            comForeignMergerVos.add(comForeignMergerVo);
        });
        companyDetailVo.setComForeignMergerVos(comForeignMergerVos);

        ComPreipoExample comPreipoExample = new ComPreipoExample();
        comPreipoExample.createCriteria().andComIdEqualTo(comId);
        List<ComPreipo> comPreipos = comPreipoMapper.selectByExample(comPreipoExample);
        List<ComPreipoVo> comPreipoVos = new ArrayList<ComPreipoVo>();
        comPreipos.forEach(comPreipo -> {
            ComPreipoVo comPreipoVo = new ComPreipoVo();
            BeanUtils.copyProperties(comPreipo, comPreipoVo);

            // 转换日期(yyyy/MM/dd)
            Date preipoTime = comPreipo.getTime();
            String new_preipoTime = DateUtils.formatDateToString(preipoTime, DateUtils.DATE_FORMAT_YMD);
            comPreipoVo.setTime(new_preipoTime);

            comPreipoVos.add(comPreipoVo);
        });
        companyDetailVo.setComPreipoVos(comPreipoVos);


        List<ComBequiteventVo> comBequiteventVos = new ArrayList<ComBequiteventVo>();
        ComBequiteventExample comBequiteventExample = new ComBequiteventExample();
        comBequiteventExample.createCriteria().andComIdEqualTo(comId);
        List<ComBequitevent> comBequitevents = comBequiteventMapper.selectByExample(comBequiteventExample);
        comBequitevents.forEach(comBequitevent -> {
            ComBequiteventVo comBequiteventVo = new ComBequiteventVo();
            BeanUtils.copyProperties(comBequitevent, comBequiteventVo);
            // 转换日期格式
            Date quitTime = comBequitevent.getQuitTime();
            String new_quitTime = DateUtils.formatDateToString(quitTime, DateUtils.DATE_FORMAT_YMD);
            comBequiteventVo.setQuitTime(new_quitTime);

            Integer quitId = comBequiteventVo.getQuitId();
            ComBequiteventExtraExample comBequiteventExtraExample = new ComBequiteventExtraExample();
            comBequiteventExtraExample.createCriteria().andQuitIdEqualTo(quitId);
            List<ComBequiteventExtra> comBequiteventExtras = comBequiteventExtraMapper.selectByExample(comBequiteventExtraExample);
            List<ComBequiteventExtraVo> comBequiteventExtraVos = new ArrayList<ComBequiteventExtraVo>();
            comBequiteventExtras.forEach(comBequiteventExtra -> {
                ComBequiteventExtraVo comBequiteventExtraVo = new ComBequiteventExtraVo();
                BeanUtils.copyProperties(comBequiteventExtra, comBequiteventExtraVo);
                comBequiteventExtraVos.add(comBequiteventExtraVo);
            });
            comBequiteventVo.setComBequiteventExtraVos(comBequiteventExtraVos);
            comBequiteventVos.add(comBequiteventVo);
        });
        companyDetailVo.setComBequiteventVos(comBequiteventVos);

        ComTeamExample comTeamExample = new ComTeamExample();
        comTeamExample.createCriteria().andComIdEqualTo(comId);
        List<ComTeam> comTeams = comTeamMapper.selectByExample(comTeamExample);
        List<ComTeamVo> comTeamVos = new ArrayList<ComTeamVo>();
        comTeams.forEach(comTeam -> {
            ComTeamVo comTeamVo = new ComTeamVo();
            BeanUtils.copyProperties(comTeam, comTeamVo);
            comTeamVos.add(comTeamVo);
        });
        companyDetailVo.setComTeamVos(comTeamVos);


        ComProductExample comProductExample = new ComProductExample();
        comProductExample.createCriteria().andComIdEqualTo(comId);
        List<ComProduct> comProducts = comProductMapper.selectByExample(comProductExample);
        List<ComProductVo> comProductVos = new ArrayList<ComProductVo>();
        comProducts.forEach(comProduct -> {
            ComProductVo comProductVo = new ComProductVo();
            BeanUtils.copyProperties(comProduct, comProductVo);
            comProductVos.add(comProductVo);
        });
        companyDetailVo.setComProductVos(comProductVos);


        ComNewsExample comNewsExample = new ComNewsExample();
        comNewsExample.createCriteria().andComIdEqualTo(comId);
        List<ComNews> comNews = comNewsMapper.selectByExample(comNewsExample);
        List<ComNewsVo> comNewsVos = new ArrayList<ComNewsVo>();
        comNews.forEach(comNew -> {
            ComNewsVo comNewsVo = new ComNewsVo();
            BeanUtils.copyProperties(comNew, comNewsVo);
            comNewsVos.add(comNewsVo);
        });
        companyDetailVo.setComNewsVos(comNewsVos);

        ComMilestoneExample comMilestoneExample = new ComMilestoneExample();
        comMilestoneExample.createCriteria().andComIdEqualTo(comId);
        List<ComMilestone> comMilestones = comMilestoneMapper.selectByExample(comMilestoneExample);
        List<ComMilestoneVo> comMilestoneVos = new ArrayList<ComMilestoneVo>();
        comMilestones.forEach(comMilestone -> {
            ComMilestoneVo comMilestoneVo = new ComMilestoneVo();
            BeanUtils.copyProperties(comMilestone, comMilestoneVo);
            comMilestoneVos.add(comMilestoneVo);
        });
        companyDetailVo.setComMilestoneVos(comMilestoneVos);

        return companyDetailVo;
    }
}
