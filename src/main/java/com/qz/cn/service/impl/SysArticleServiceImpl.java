package com.qz.cn.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qz.cn.dto.SysArticle;
import com.qz.cn.dto.SysArticleExample;
import com.qz.cn.dto.SysSecond;
import com.qz.cn.mapper.SysArticleMapper;
import com.qz.cn.mapper.SysSecondMapper;
import com.qz.cn.service.SysArticleService;
import com.qz.cn.vo.ArticleVo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.schema.Example;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 文章的业务持处理层
 */
@Service
public class SysArticleServiceImpl implements SysArticleService {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式

    @Autowired
    private SysArticleMapper sysArticleMapper;
    @Autowired
    private SysSecondMapper sysSecondMapper;

    @Override
    public List<ArticleVo> queryArticle(Integer page,Integer size) {
        SysArticleExample articleExample=new SysArticleExample();
        articleExample.setOrderByClause("article_id desc");
        articleExample.createCriteria().andIsDelEqualTo(0).andIsRotationNotEqualTo(1);
        List<SysArticle> total= sysArticleMapper.selectByExampleWithBLOBs(articleExample);
        PageHelper.startPage(page,size);
      List<SysArticle> list= sysArticleMapper.selectByExampleWithBLOBs(articleExample);
      List<ArticleVo> ArticleVos = new ArrayList<ArticleVo>();
      list.forEach(sysArticle -> {
          ArticleVo articleVo=new ArticleVo();
          if (sysArticle.getSecondId()!=null){
              SysSecond sysSecond= sysSecondMapper.selectByPrimaryKey(sysArticle.getSecondId());
              articleVo.setSecondId(sysSecond.getSecondName());
          }else{
              articleVo.setSecondId("未定义");
          }
          if (sysArticle.getCreateTime()!=null){
              System.out.println(sysArticle.getCreateTime());
              String time=sdf.format(sysArticle.getCreateTime());
              articleVo.setCreateTime(time);
          }


          articleVo.setTotal(total.size());
          BeanUtils.copyProperties(sysArticle,articleVo);
          ArticleVos.add(articleVo);
      });
        return ArticleVos;
    }

    @Override
    public List<ArticleVo> queryArticles(Integer page, Integer size) {
        SysArticleExample articleExample=new SysArticleExample();
        articleExample.setOrderByClause("article_id desc");
        articleExample.createCriteria().andSecondIdNotEqualTo(9).andIsDelEqualTo(0).andIsRotationNotEqualTo(1);
        PageHelper.startPage(page,size);
        List<SysArticle> list= sysArticleMapper.selectByExampleWithBLOBs(articleExample);
        List<ArticleVo> ArticleVos = new ArrayList<ArticleVo>();
        list.forEach(sysArticle -> {
            ArticleVo articleVo=new ArticleVo();
            if (sysArticle.getSecondId()!=null){
                SysSecond sysSecond= sysSecondMapper.selectByPrimaryKey(sysArticle.getSecondId());


                articleVo.setSecondId(sysSecond.getSecondName());
            }else{
                articleVo.setSecondId("未定义");
            }



            if (sysArticle.getCreateTime()!=null){
                String time=sdf.format(sysArticle.getCreateTime());
                articleVo.setCreateTime(time);
            }
            BeanUtils.copyProperties(sysArticle,articleVo);
            ArticleVos.add(articleVo);
        });
        return ArticleVos;
    }

    @Override
    public ArticleVo queryArticleByArtcileId(Integer id) {
           SysArticle article= sysArticleMapper.selectByPrimaryKey(id);
        ArticleVo articleVo=new ArticleVo();
           if(article.getSecondId()!=null){
               SysSecond sysSecond= sysSecondMapper.selectByPrimaryKey(article.getSecondId());
               articleVo.setSecondId(sysSecond.getSecondName());
           }else{
               articleVo.setSecondId("未定义");
           }

            BeanUtils.copyProperties(article,articleVo);
            return articleVo;
    }

    @Override
    public List<ArticleVo> queryArticleByMenuId(Integer id,Integer page,Integer size) {
        SysArticleExample articleExample=new SysArticleExample();
        articleExample.setOrderByClause("article_id asc");
        articleExample.createCriteria().andIsDelEqualTo(0).andSecondIdEqualTo(id).andIsRotationNotEqualTo(1);
        List<SysArticle> list1=sysArticleMapper.selectByExampleWithBLOBs(articleExample);
        PageHelper.startPage(page,size);
        List<SysArticle> list=sysArticleMapper.selectByExampleWithBLOBs(articleExample);
        List<ArticleVo> ArticleVos = new ArrayList<ArticleVo>();
        list.forEach(sysArticle -> {
            ArticleVo articleVo=new ArticleVo();
            if(sysArticle.getSecondId()!=null){
                SysSecond sysSecond= sysSecondMapper.selectByPrimaryKey(sysArticle.getSecondId());
                articleVo.setSecondId(sysSecond.getSecondName());
            }else{
                articleVo.setSecondId("未定义");
            }
            if (sysArticle.getCreateTime()!=null){
                String time=sdf.format(sysArticle.getCreateTime());
                articleVo.setCreateTime(time);
            }
            articleVo.setTotal(list1.size());
            BeanUtils.copyProperties(sysArticle,articleVo);
            ArticleVos.add(articleVo);

        });
        return ArticleVos;
    }

    @Override
    public List<ArticleVo> queryTitleImg() {
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式

        SysArticleExample articleExample=new SysArticleExample();
        articleExample.createCriteria().andIsRotationEqualTo(1).andIsDelEqualTo(0);
           List<SysArticle> article= sysArticleMapper.selectByExample(articleExample);
           List<ArticleVo> articleVos=new ArrayList<>();
           article.forEach(articles  ->{
               ArticleVo articleVo=new ArticleVo();
               if (articles.getCreateTime()!=null){
                   String time=sdf.format(articles.getCreateTime());
                   articleVo.setCreateTime(time);
               }
               BeanUtils.copyProperties(articles,articleVo);
               articleVos.add(articleVo);
           });
        return articleVos;
    }


}
