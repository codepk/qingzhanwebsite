package com.qz.cn.service.impl;

import com.qz.cn.dto.*;
import com.qz.cn.mapper.SysArticleMapper;
import com.qz.cn.mapper.SysSecondMapper;
import com.qz.cn.mapper.SysTypeMapper;
import com.qz.cn.service.WebsiteService;
import com.qz.cn.vo.ArticleVo;
import com.qz.cn.vo.SecondVo;
import com.qz.cn.vo.TypeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pankai
 * @create 2020-11-10 13:05
 */
@Service
public class WebsiteServiceImpl implements WebsiteService {

    @Autowired
    private SysTypeMapper sysTypeMapper;

    @Autowired
    private SysSecondMapper sysSecondMapper;

    @Autowired
    private SysArticleMapper sysArticleMapper;

    @Override
    public List<TypeVo> queryType() {
        SysTypeExample typeExample = new SysTypeExample();
        List<SysType> sysTypes = sysTypeMapper.selectByExample(typeExample);
        List<TypeVo> typeVos = new ArrayList<TypeVo>();
        sysTypes.forEach(type -> {
            TypeVo typeVo = new TypeVo();
            BeanUtils.copyProperties(type, typeVo);
            typeVos.add(typeVo);
        });
        return typeVos;
    }

    @Override
    public List<SecondVo> queryMenu(Integer typeId) {
        SysSecondExample sysSecondExample = new SysSecondExample();
        sysSecondExample.createCriteria().andTypeIdEqualTo(typeId);
        List<SysSecond> sysSeconds = sysSecondMapper.selectByExample(sysSecondExample);
        List<SecondVo> secondVos = new ArrayList<SecondVo>();
        sysSeconds.forEach(second -> {
            SecondVo secondVo = new SecondVo();
            BeanUtils.copyProperties(second, secondVo);
            secondVos.add(secondVo);
        });
        return secondVos;
    }

    @Override
    public List<ArticleVo> queryArticle() {
        SysArticleExample sysArticleExample = new SysArticleExample();
        List<SysArticle> sysArticles = sysArticleMapper.selectByExampleWithBLOBs(sysArticleExample);
        List<ArticleVo> articleVos = new ArrayList<ArticleVo>();
        sysArticles.forEach(sysArticle -> {
            ArticleVo articleVo = new ArticleVo();
            BeanUtils.copyProperties(sysArticle,articleVo);
            articleVos.add(articleVo);
        });
        return articleVos;
    }
}
