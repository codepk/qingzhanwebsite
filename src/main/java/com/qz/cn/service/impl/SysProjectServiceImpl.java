package com.qz.cn.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qz.cn.dto.SysProject;
import com.qz.cn.dto.SysProjectExample;
import com.qz.cn.dto.SysSecond;
import com.qz.cn.mapper.SysProjectMapper;
import com.qz.cn.service.SysProjectService;
import com.qz.cn.vo.ArticleVo;
import com.qz.cn.vo.ProjectVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 融资项目
 */
@Service
public class SysProjectServiceImpl implements SysProjectService {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式

    @Autowired
    private SysProjectMapper sysProjectMapper;
    @Override
    public List<ProjectVo> queryAll(Integer page,Integer size) {
        SysProjectExample sysProjectExample=new SysProjectExample();
        sysProjectExample.setOrderByClause("create_time desc");
//        sysProjectExample.createCriteria().andProjectNameLike("%"+name+"%");
        List<SysProject> list= sysProjectMapper.selectByExample(sysProjectExample);
        Page<SysProject> pages= PageHelper.startPage(page,size);
         List<SysProject> sysProjects= sysProjectMapper.selectByExample(sysProjectExample);
         List<ProjectVo> projectVos=new ArrayList<ProjectVo>();
         sysProjects.forEach(sysProject -> {
             ProjectVo projectVo=new ProjectVo();
             if (sysProject.getCreateTime()!=null){
                 String time=sdf.format(sysProject.getCreateTime());
                 projectVo.setReleaseTime(time);
             }
             BeanUtils.copyProperties(sysProject,projectVo);
             projectVo.setTotal(list.size());
             projectVos.add(projectVo);
         });
        return projectVos;
    }

    @Override
    public List<ProjectVo> queryAlls(Integer page, Integer size) {
        SysProjectExample sysProjectExample=new SysProjectExample();
        sysProjectExample.setOrderByClause("create_time desc");
        List<SysProject> list= sysProjectMapper.selectByExample(sysProjectExample);
        Page<SysProject> pages= PageHelper.startPage(page,size);
        List<SysProject> sysProjects= sysProjectMapper.selectByExample(sysProjectExample);
        List<ProjectVo> projectVos=new ArrayList<ProjectVo>();
        sysProjects.forEach(sysProject -> {
            ProjectVo projectVo=new ProjectVo();
//            if(sysProject.getProjectName().length()>5){
//                String projectName=sysProject.getProjectName();
//                System.out.println(projectName);
//              String newName= projectName.substring(0,5);
//              sysProject.setProjectName(newName);
//            }
//            if (sysProject.getIndustryName().length()>5){
//                String name=sysProject.getIndustryName();
//                System.out.println(name);
//                String newName=name.substring(0,3);
//                sysProject.setIndustryName(newName);
//            }


            if (sysProject.getCreateTime()!=null){
                String time=sdf.format(sysProject.getCreateTime());
                projectVo.setReleaseTime(time);
            }
            BeanUtils.copyProperties(sysProject,projectVo);
            projectVo.setTotal(list.size());
            projectVos.add(projectVo);
        });
        return projectVos;
    }

    @Override
    public List<ProjectVo> querySearch(Integer page, Integer size, String name) {
        SysProjectExample sysProjectExample=new SysProjectExample();
        sysProjectExample.setOrderByClause("create_time desc");

        sysProjectExample.createCriteria().andProjectNameLike("%"+name+"%");
        List<SysProject> list= sysProjectMapper.selectByExample(sysProjectExample);
        Page<SysProject> pages= PageHelper.startPage(page,size);
        List<SysProject> sysProjects= sysProjectMapper.selectByExample(sysProjectExample);
        List<ProjectVo> projectVos=new ArrayList<ProjectVo>();
        sysProjects.forEach(sysProject -> {
            ProjectVo projectVo=new ProjectVo();
            if (sysProject.getCreateTime()!=null){
                String time=sdf.format(sysProject.getCreateTime());
                projectVo.setReleaseTime(time);
            }
            BeanUtils.copyProperties(sysProject,projectVo);
            projectVo.setTotal(list.size());
            projectVos.add(projectVo);
        });
        return projectVos;
    }

    @Override
    public ProjectVo queryProjectByProjectId(Integer id) {
        SysProject sysProject=sysProjectMapper.selectByPrimaryKey(id);
        ProjectVo projectVo =new ProjectVo();
        BeanUtils.copyProperties(sysProject,projectVo);
        if(sysProject.getCreateTime()!=null){
            String time=sdf.format(sysProject.getCreateTime());
            projectVo.setReleaseTime(time);
        }

        return projectVo;
    }



}
