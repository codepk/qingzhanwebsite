package com.qz.cn.service.impl;

import com.qz.cn.dto.SysVisitor;
import com.qz.cn.dto.SysVisitorExample;
import com.qz.cn.mapper.SysVisitorMapper;
import com.qz.cn.param.VistitorParam;
import com.qz.cn.service.SysVisitorService;
import com.qz.cn.utils.Md5Utils;
import com.qz.cn.vo.VisitorVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author pankai
 * @create 2020-11-27 13:42
 */
@Service
public class SysVisitorServiceImpl implements SysVisitorService {

    @Autowired
    private SysVisitorMapper sysVisitorMapper;

    @Override
    public boolean addVisitor(VistitorParam vistitorParam) {
        boolean flag = false;
        String visitorName = vistitorParam.getVisitorName();
        SysVisitorExample sysVisitorExample = new SysVisitorExample();
        sysVisitorExample.createCriteria().andVisitorNameEqualTo(visitorName);
        List<SysVisitor> sysVisitors = sysVisitorMapper.selectByExample(sysVisitorExample);
        if (null == sysVisitors || sysVisitors.size() == 0) {
            SysVisitor sysVisitor = new SysVisitor();
            BeanUtils.copyProperties(vistitorParam, sysVisitor);
            String visitorPassword = sysVisitor.getVisitorPassword();
            String md5Password = Md5Utils.md5(visitorPassword);
            sysVisitor.setVisitorPassword(md5Password);
            sysVisitor.setVisitorPhone(vistitorParam.getVisitorName());
            sysVisitor.setCreateTime(new Date());
            sysVisitor.setUpdateTime(new Date());
            int i = sysVisitorMapper.insertSelective(sysVisitor);
            if (i > 0) {
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public VisitorVo loginByPhone(String userName, String password) {
        String md5Password = Md5Utils.md5(password);
        VisitorVo visitorVo = new VisitorVo();
        SysVisitorExample sysVisitorExample = new SysVisitorExample();
        sysVisitorExample.createCriteria().andVisitorNameEqualTo(userName).andVisitorPasswordEqualTo(md5Password).andIsDelEqualTo(0);
        List<SysVisitor> sysVisitors = sysVisitorMapper.selectByExample(sysVisitorExample);
        SysVisitor sysVisitor = sysVisitors.get(0);
        if (sysVisitor!=null){
            BeanUtils.copyProperties(sysVisitor, visitorVo);
            return visitorVo;
        }
        return null;
    }

    @Override
    public VisitorVo loginByEmail(String visitorEmail, String password) {
        String md5Password = Md5Utils.md5(password);
        VisitorVo visitorVo = new VisitorVo();
        SysVisitorExample sysVisitorExample = new SysVisitorExample();
        sysVisitorExample.createCriteria().andVisitorEmailEqualTo(visitorEmail).andVisitorPasswordEqualTo(md5Password).andIsDelEqualTo(0);
        List<SysVisitor> sysVisitors = sysVisitorMapper.selectByExample(sysVisitorExample);
        SysVisitor sysVisitor = sysVisitors.get(0);
        if (sysVisitor!=null){
            BeanUtils.copyProperties(sysVisitor, visitorVo);
            return visitorVo;
        }
        return null;
    }
}
