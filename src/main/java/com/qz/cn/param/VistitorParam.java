package com.qz.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pankai
 * @create 2020-11-27 11:24
 */
@ApiModel("注册传输实体类")
@Data
public class VistitorParam implements Serializable {

    @ApiModelProperty("注册密码")
    private String visitorPassword;
    @ApiModelProperty("注册用户名(手机号)")
    private String visitorName;
    @ApiModelProperty("注册邮箱")
    private String visitorEmail;

}
