package com.qz.cn.mapper;

import com.qz.cn.dto.SysProduct;
import com.qz.cn.dto.SysProductExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysProductMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    long countByExample(SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int deleteByExample(SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer productId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int insert(SysProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int insertSelective(SysProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    List<SysProduct> selectByExampleWithBLOBs(SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    List<SysProduct> selectByExample(SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    SysProduct selectByPrimaryKey(Integer productId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysProduct record, @Param("example") SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByExampleWithBLOBs(@Param("record") SysProduct record, @Param("example") SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysProduct record, @Param("example") SysProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SysProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(SysProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_product
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SysProduct record);
}