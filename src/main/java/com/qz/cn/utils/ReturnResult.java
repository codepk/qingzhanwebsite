package com.qz.cn.utils;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReturnResult<T> implements Serializable {
    private String msg;
    private int code;
    private T data;

}
