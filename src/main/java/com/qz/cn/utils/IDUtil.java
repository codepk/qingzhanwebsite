package com.qz.cn.utils;

import java.util.Random;
import java.util.UUID;

public class IDUtil {
    /**
     * 取得UUID
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成随机字符串,subString包前不包后
     * @return
     */
    public static String createNonceStr(){
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0,31);
    }

    /**
     * 取得6位随机数
     * @return
     */
    public static String getCode(){
        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            code.append(Integer.valueOf(random.nextInt(10)).toString());
        }
        return code.toString();
    }
    /**
     * 取得随机ID 6时间戳+6UUID+6随机数
     *
     * @return
     */
    public static String getId() {
        String id = String.valueOf(System.currentTimeMillis()).substring(0, 6) + getUUID().substring(0, 6) + getCode();
        return id;
    }
}
