package com.qz.cn.utils;

import java.text.DecimalFormat;

/**
 * 除法工具类
 * @author pankai
 * @create 2020-12-31 9:40
 */
public class DivisionUtils {

    /**
     * 两数相除保留一位小数
     * @param a
     * @param b
     * @return
     */
    public static String division(int a ,int b){
        String result = "";
        float num =(float)a/b;

        DecimalFormat df = new DecimalFormat("0.0");

        result = df.format(num);

        return result;

    }
}
