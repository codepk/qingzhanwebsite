package com.qz.cn.utils;

public class ReturnResultUtils {

    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMsg("success");
        returnResult.setCode(600);
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(T data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMsg("success");
        returnResult.setCode(600);
        returnResult.setData(data);
        return returnResult;
    }


    public static ReturnResult returnFail(int code) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMsg("fail");
        returnResult.setCode(code);
        return returnResult;
    }
    public static ReturnResult returnFail(int code,String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMsg(msg);
        returnResult.setCode(code);
        return returnResult;
    }
}
