package com.qz.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class ComTeamExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table com_team
     *
     * @mbg.generated
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table com_team
     *
     * @mbg.generated
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table com_team
     *
     * @mbg.generated
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public ComTeamExample() {
        oredCriteria = new ArrayList<>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table com_team
     *
     * @mbg.generated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andComIdIsNull() {
            addCriterion("com_id is null");
            return (Criteria) this;
        }

        public Criteria andComIdIsNotNull() {
            addCriterion("com_id is not null");
            return (Criteria) this;
        }

        public Criteria andComIdEqualTo(Integer value) {
            addCriterion("com_id =", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdNotEqualTo(Integer value) {
            addCriterion("com_id <>", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdGreaterThan(Integer value) {
            addCriterion("com_id >", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("com_id >=", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdLessThan(Integer value) {
            addCriterion("com_id <", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdLessThanOrEqualTo(Integer value) {
            addCriterion("com_id <=", value, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdIn(List<Integer> values) {
            addCriterion("com_id in", values, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdNotIn(List<Integer> values) {
            addCriterion("com_id not in", values, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdBetween(Integer value1, Integer value2) {
            addCriterion("com_id between", value1, value2, "comId");
            return (Criteria) this;
        }

        public Criteria andComIdNotBetween(Integer value1, Integer value2) {
            addCriterion("com_id not between", value1, value2, "comId");
            return (Criteria) this;
        }

        public Criteria andPerIdIsNull() {
            addCriterion("per_id is null");
            return (Criteria) this;
        }

        public Criteria andPerIdIsNotNull() {
            addCriterion("per_id is not null");
            return (Criteria) this;
        }

        public Criteria andPerIdEqualTo(Integer value) {
            addCriterion("per_id =", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdNotEqualTo(Integer value) {
            addCriterion("per_id <>", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdGreaterThan(Integer value) {
            addCriterion("per_id >", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("per_id >=", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdLessThan(Integer value) {
            addCriterion("per_id <", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdLessThanOrEqualTo(Integer value) {
            addCriterion("per_id <=", value, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdIn(List<Integer> values) {
            addCriterion("per_id in", values, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdNotIn(List<Integer> values) {
            addCriterion("per_id not in", values, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdBetween(Integer value1, Integer value2) {
            addCriterion("per_id between", value1, value2, "perId");
            return (Criteria) this;
        }

        public Criteria andPerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("per_id not between", value1, value2, "perId");
            return (Criteria) this;
        }

        public Criteria andPerNameIsNull() {
            addCriterion("per_name is null");
            return (Criteria) this;
        }

        public Criteria andPerNameIsNotNull() {
            addCriterion("per_name is not null");
            return (Criteria) this;
        }

        public Criteria andPerNameEqualTo(String value) {
            addCriterion("per_name =", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameNotEqualTo(String value) {
            addCriterion("per_name <>", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameGreaterThan(String value) {
            addCriterion("per_name >", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameGreaterThanOrEqualTo(String value) {
            addCriterion("per_name >=", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameLessThan(String value) {
            addCriterion("per_name <", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameLessThanOrEqualTo(String value) {
            addCriterion("per_name <=", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameLike(String value) {
            addCriterion("per_name like", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameNotLike(String value) {
            addCriterion("per_name not like", value, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameIn(List<String> values) {
            addCriterion("per_name in", values, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameNotIn(List<String> values) {
            addCriterion("per_name not in", values, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameBetween(String value1, String value2) {
            addCriterion("per_name between", value1, value2, "perName");
            return (Criteria) this;
        }

        public Criteria andPerNameNotBetween(String value1, String value2) {
            addCriterion("per_name not between", value1, value2, "perName");
            return (Criteria) this;
        }

        public Criteria andPerLogoIsNull() {
            addCriterion("per_logo is null");
            return (Criteria) this;
        }

        public Criteria andPerLogoIsNotNull() {
            addCriterion("per_logo is not null");
            return (Criteria) this;
        }

        public Criteria andPerLogoEqualTo(String value) {
            addCriterion("per_logo =", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoNotEqualTo(String value) {
            addCriterion("per_logo <>", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoGreaterThan(String value) {
            addCriterion("per_logo >", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoGreaterThanOrEqualTo(String value) {
            addCriterion("per_logo >=", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoLessThan(String value) {
            addCriterion("per_logo <", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoLessThanOrEqualTo(String value) {
            addCriterion("per_logo <=", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoLike(String value) {
            addCriterion("per_logo like", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoNotLike(String value) {
            addCriterion("per_logo not like", value, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoIn(List<String> values) {
            addCriterion("per_logo in", values, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoNotIn(List<String> values) {
            addCriterion("per_logo not in", values, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoBetween(String value1, String value2) {
            addCriterion("per_logo between", value1, value2, "perLogo");
            return (Criteria) this;
        }

        public Criteria andPerLogoNotBetween(String value1, String value2) {
            addCriterion("per_logo not between", value1, value2, "perLogo");
            return (Criteria) this;
        }

        public Criteria andDesIsNull() {
            addCriterion("des is null");
            return (Criteria) this;
        }

        public Criteria andDesIsNotNull() {
            addCriterion("des is not null");
            return (Criteria) this;
        }

        public Criteria andDesEqualTo(String value) {
            addCriterion("des =", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotEqualTo(String value) {
            addCriterion("des <>", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesGreaterThan(String value) {
            addCriterion("des >", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesGreaterThanOrEqualTo(String value) {
            addCriterion("des >=", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLessThan(String value) {
            addCriterion("des <", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLessThanOrEqualTo(String value) {
            addCriterion("des <=", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLike(String value) {
            addCriterion("des like", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotLike(String value) {
            addCriterion("des not like", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesIn(List<String> values) {
            addCriterion("des in", values, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotIn(List<String> values) {
            addCriterion("des not in", values, "des");
            return (Criteria) this;
        }

        public Criteria andDesBetween(String value1, String value2) {
            addCriterion("des between", value1, value2, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotBetween(String value1, String value2) {
            addCriterion("des not between", value1, value2, "des");
            return (Criteria) this;
        }

        public Criteria andPerWeiboIsNull() {
            addCriterion("per_weibo is null");
            return (Criteria) this;
        }

        public Criteria andPerWeiboIsNotNull() {
            addCriterion("per_weibo is not null");
            return (Criteria) this;
        }

        public Criteria andPerWeiboEqualTo(String value) {
            addCriterion("per_weibo =", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboNotEqualTo(String value) {
            addCriterion("per_weibo <>", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboGreaterThan(String value) {
            addCriterion("per_weibo >", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboGreaterThanOrEqualTo(String value) {
            addCriterion("per_weibo >=", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboLessThan(String value) {
            addCriterion("per_weibo <", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboLessThanOrEqualTo(String value) {
            addCriterion("per_weibo <=", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboLike(String value) {
            addCriterion("per_weibo like", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboNotLike(String value) {
            addCriterion("per_weibo not like", value, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboIn(List<String> values) {
            addCriterion("per_weibo in", values, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboNotIn(List<String> values) {
            addCriterion("per_weibo not in", values, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboBetween(String value1, String value2) {
            addCriterion("per_weibo between", value1, value2, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerWeiboNotBetween(String value1, String value2) {
            addCriterion("per_weibo not between", value1, value2, "perWeibo");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinIsNull() {
            addCriterion("per_linkedin is null");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinIsNotNull() {
            addCriterion("per_linkedin is not null");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinEqualTo(String value) {
            addCriterion("per_linkedin =", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinNotEqualTo(String value) {
            addCriterion("per_linkedin <>", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinGreaterThan(String value) {
            addCriterion("per_linkedin >", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinGreaterThanOrEqualTo(String value) {
            addCriterion("per_linkedin >=", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinLessThan(String value) {
            addCriterion("per_linkedin <", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinLessThanOrEqualTo(String value) {
            addCriterion("per_linkedin <=", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinLike(String value) {
            addCriterion("per_linkedin like", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinNotLike(String value) {
            addCriterion("per_linkedin not like", value, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinIn(List<String> values) {
            addCriterion("per_linkedin in", values, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinNotIn(List<String> values) {
            addCriterion("per_linkedin not in", values, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinBetween(String value1, String value2) {
            addCriterion("per_linkedin between", value1, value2, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andPerLinkedinNotBetween(String value1, String value2) {
            addCriterion("per_linkedin not between", value1, value2, "perLinkedin");
            return (Criteria) this;
        }

        public Criteria andIntroduceIsNull() {
            addCriterion("introduce is null");
            return (Criteria) this;
        }

        public Criteria andIntroduceIsNotNull() {
            addCriterion("introduce is not null");
            return (Criteria) this;
        }

        public Criteria andIntroduceEqualTo(String value) {
            addCriterion("introduce =", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceNotEqualTo(String value) {
            addCriterion("introduce <>", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceGreaterThan(String value) {
            addCriterion("introduce >", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceGreaterThanOrEqualTo(String value) {
            addCriterion("introduce >=", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceLessThan(String value) {
            addCriterion("introduce <", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceLessThanOrEqualTo(String value) {
            addCriterion("introduce <=", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceLike(String value) {
            addCriterion("introduce like", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceNotLike(String value) {
            addCriterion("introduce not like", value, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceIn(List<String> values) {
            addCriterion("introduce in", values, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceNotIn(List<String> values) {
            addCriterion("introduce not in", values, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceBetween(String value1, String value2) {
            addCriterion("introduce between", value1, value2, "introduce");
            return (Criteria) this;
        }

        public Criteria andIntroduceNotBetween(String value1, String value2) {
            addCriterion("introduce not between", value1, value2, "introduce");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyIsNull() {
            addCriterion("is_incumbency is null");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyIsNotNull() {
            addCriterion("is_incumbency is not null");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyEqualTo(String value) {
            addCriterion("is_incumbency =", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyNotEqualTo(String value) {
            addCriterion("is_incumbency <>", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyGreaterThan(String value) {
            addCriterion("is_incumbency >", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyGreaterThanOrEqualTo(String value) {
            addCriterion("is_incumbency >=", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyLessThan(String value) {
            addCriterion("is_incumbency <", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyLessThanOrEqualTo(String value) {
            addCriterion("is_incumbency <=", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyLike(String value) {
            addCriterion("is_incumbency like", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyNotLike(String value) {
            addCriterion("is_incumbency not like", value, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyIn(List<String> values) {
            addCriterion("is_incumbency in", values, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyNotIn(List<String> values) {
            addCriterion("is_incumbency not in", values, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyBetween(String value1, String value2) {
            addCriterion("is_incumbency between", value1, value2, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andIsIncumbencyNotBetween(String value1, String value2) {
            addCriterion("is_incumbency not between", value1, value2, "isIncumbency");
            return (Criteria) this;
        }

        public Criteria andEducationIsNull() {
            addCriterion("education is null");
            return (Criteria) this;
        }

        public Criteria andEducationIsNotNull() {
            addCriterion("education is not null");
            return (Criteria) this;
        }

        public Criteria andEducationEqualTo(String value) {
            addCriterion("education =", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotEqualTo(String value) {
            addCriterion("education <>", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThan(String value) {
            addCriterion("education >", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThanOrEqualTo(String value) {
            addCriterion("education >=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThan(String value) {
            addCriterion("education <", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThanOrEqualTo(String value) {
            addCriterion("education <=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLike(String value) {
            addCriterion("education like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotLike(String value) {
            addCriterion("education not like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationIn(List<String> values) {
            addCriterion("education in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotIn(List<String> values) {
            addCriterion("education not in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationBetween(String value1, String value2) {
            addCriterion("education between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotBetween(String value1, String value2) {
            addCriterion("education not between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEverjobIsNull() {
            addCriterion("everjob is null");
            return (Criteria) this;
        }

        public Criteria andEverjobIsNotNull() {
            addCriterion("everjob is not null");
            return (Criteria) this;
        }

        public Criteria andEverjobEqualTo(String value) {
            addCriterion("everjob =", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobNotEqualTo(String value) {
            addCriterion("everjob <>", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobGreaterThan(String value) {
            addCriterion("everjob >", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobGreaterThanOrEqualTo(String value) {
            addCriterion("everjob >=", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobLessThan(String value) {
            addCriterion("everjob <", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobLessThanOrEqualTo(String value) {
            addCriterion("everjob <=", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobLike(String value) {
            addCriterion("everjob like", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobNotLike(String value) {
            addCriterion("everjob not like", value, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobIn(List<String> values) {
            addCriterion("everjob in", values, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobNotIn(List<String> values) {
            addCriterion("everjob not in", values, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobBetween(String value1, String value2) {
            addCriterion("everjob between", value1, value2, "everjob");
            return (Criteria) this;
        }

        public Criteria andEverjobNotBetween(String value1, String value2) {
            addCriterion("everjob not between", value1, value2, "everjob");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table com_team
     *
     * @mbg.generated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table com_team
     *
     * @mbg.generated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}