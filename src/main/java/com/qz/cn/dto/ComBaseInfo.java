package com.qz.cn.dto;

import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table com_base_info
 */
public class ComBaseInfo {
    /**
     * Database Column Remarks:
     *   基本信息表自增ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   公司ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_id
     *
     * @mbg.generated
     */
    private Integer comId;

    /**
     * Database Column Remarks:
     *   公司名称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_name
     *
     * @mbg.generated
     */
    private String comName;

    /**
     * Database Column Remarks:
     *   公司简称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_sec_name
     *
     * @mbg.generated
     */
    private String comSecName;

    /**
     * Database Column Remarks:
     *   公司注册名称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_registered_name
     *
     * @mbg.generated
     */
    private String comRegisteredName;

    /**
     * Database Column Remarks:
     *   一句话介绍公司
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_slogan
     *
     * @mbg.generated
     */
    private String comSlogan;

    /**
     * Database Column Remarks:
     *   公司缩略图
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_logo
     *
     * @mbg.generated
     */
    private String comLogo;

    /**
     * Database Column Remarks:
     *   公司LOGO
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_logo_archive
     *
     * @mbg.generated
     */
    private String comLogoArchive;

    /**
     * Database Column Remarks:
     *   公司视频
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_video
     *
     * @mbg.generated
     */
    private String comVideo;

    /**
     * Database Column Remarks:
     *   是否是千里马公司 0否1是
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.horse_club
     *
     * @mbg.generated
     */
    private Byte horseClub;

    /**
     * Database Column Remarks:
     *   是否是独角兽公司 0否1是
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.unicorn_club
     *
     * @mbg.generated
     */
    private Byte unicornClub;

    /**
     * Database Column Remarks:
     *   公司描述信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_des
     *
     * @mbg.generated
     */
    private String comDes;

    /**
     * Database Column Remarks:
     *   公司网址
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_url
     *
     * @mbg.generated
     */
    private String comUrl;

    /**
     * Database Column Remarks:
     *   公司创建年
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_born_year
     *
     * @mbg.generated
     */
    private Integer comBornYear;

    /**
     * Database Column Remarks:
     *   in 国内公司   out 国外公司
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_location
     *
     * @mbg.generated
     */
    private String comLocation;

    /**
     * Database Column Remarks:
     *   公司创建月
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_born_month
     *
     * @mbg.generated
     */
    private Integer comBornMonth;

    /**
     * Database Column Remarks:
     *   公司位置省份
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_prov
     *
     * @mbg.generated
     */
    private String comProv;

    /**
     * Database Column Remarks:
     *   公司位置市区
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_city
     *
     * @mbg.generated
     */
    private String comCity;

    /**
     * Database Column Remarks:
     *   公司状态信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_status_name
     *
     * @mbg.generated
     */
    private String comStatusName;

    /**
     * Database Column Remarks:
     *   关闭时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_close_date
     *
     * @mbg.generated
     */
    private String comCloseDate;

    /**
     * Database Column Remarks:
     *   公司阶段信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_stage_name
     *
     * @mbg.generated
     */
    private String comStageName;

    /**
     * Database Column Remarks:
     *   公司融资需求信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_fund_needs_name
     *
     * @mbg.generated
     */
    private String comFundNeedsName;

    /**
     * Database Column Remarks:
     *   公司规模信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_scale_name
     *
     * @mbg.generated
     */
    private String comScaleName;

    /**
     * Database Column Remarks:
     *   公司当前融资轮次信息
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_fund_status_name
     *
     * @mbg.generated
     */
    private String comFundStatusName;

    /**
     * Database Column Remarks:
     *   公司微博
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_weibo_url
     *
     * @mbg.generated
     */
    private String comWeiboUrl;

    /**
     * Database Column Remarks:
     *   公司微信
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_weixin_url
     *
     * @mbg.generated
     */
    private String comWeixinUrl;

    /**
     * Database Column Remarks:
     *   公司电话
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_cont_tel
     *
     * @mbg.generated
     */
    private String comContTel;

    /**
     * Database Column Remarks:
     *   公司邮箱
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_cont_email
     *
     * @mbg.generated
     */
    private String comContEmail;

    /**
     * Database Column Remarks:
     *   公司详细地址
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_cont_addr
     *
     * @mbg.generated
     */
    private String comContAddr;

    /**
     * Database Column Remarks:
     *   行业ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.cat_id
     *
     * @mbg.generated
     */
    private Integer catId;

    /**
     * Database Column Remarks:
     *   行业名称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.cat_name
     *
     * @mbg.generated
     */
    private String catName;

    /**
     * Database Column Remarks:
     *   子行业ID
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.sub_cat_id
     *
     * @mbg.generated
     */
    private Integer subCatId;

    /**
     * Database Column Remarks:
     *   子行业名称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.sub_cat_name
     *
     * @mbg.generated
     */
    private String subCatName;

    /**
     * Database Column Remarks:
     *   公司对应机构的id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_relative_invst
     *
     * @mbg.generated
     */
    private String comRelativeInvst;

    /**
     * Database Column Remarks:
     *   公司总融资金额
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.invse_total_money
     *
     * @mbg.generated
     */
    private String invseTotalMoney;

    /**
     * Database Column Remarks:
     *   1 #普通公司   2 #神秘公司
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.com_type
     *
     * @mbg.generated
     */
    private Byte comType;

    /**
     * Database Column Remarks:
     *   录入时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * Database Column Remarks:
     *   修改时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column com_base_info.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.id
     *
     * @return the value of com_base_info.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.id
     *
     * @param id the value for com_base_info.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_id
     *
     * @return the value of com_base_info.com_id
     *
     * @mbg.generated
     */
    public Integer getComId() {
        return comId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_id
     *
     * @param comId the value for com_base_info.com_id
     *
     * @mbg.generated
     */
    public void setComId(Integer comId) {
        this.comId = comId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_name
     *
     * @return the value of com_base_info.com_name
     *
     * @mbg.generated
     */
    public String getComName() {
        return comName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_name
     *
     * @param comName the value for com_base_info.com_name
     *
     * @mbg.generated
     */
    public void setComName(String comName) {
        this.comName = comName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_sec_name
     *
     * @return the value of com_base_info.com_sec_name
     *
     * @mbg.generated
     */
    public String getComSecName() {
        return comSecName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_sec_name
     *
     * @param comSecName the value for com_base_info.com_sec_name
     *
     * @mbg.generated
     */
    public void setComSecName(String comSecName) {
        this.comSecName = comSecName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_registered_name
     *
     * @return the value of com_base_info.com_registered_name
     *
     * @mbg.generated
     */
    public String getComRegisteredName() {
        return comRegisteredName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_registered_name
     *
     * @param comRegisteredName the value for com_base_info.com_registered_name
     *
     * @mbg.generated
     */
    public void setComRegisteredName(String comRegisteredName) {
        this.comRegisteredName = comRegisteredName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_slogan
     *
     * @return the value of com_base_info.com_slogan
     *
     * @mbg.generated
     */
    public String getComSlogan() {
        return comSlogan;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_slogan
     *
     * @param comSlogan the value for com_base_info.com_slogan
     *
     * @mbg.generated
     */
    public void setComSlogan(String comSlogan) {
        this.comSlogan = comSlogan;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_logo
     *
     * @return the value of com_base_info.com_logo
     *
     * @mbg.generated
     */
    public String getComLogo() {
        return comLogo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_logo
     *
     * @param comLogo the value for com_base_info.com_logo
     *
     * @mbg.generated
     */
    public void setComLogo(String comLogo) {
        this.comLogo = comLogo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_logo_archive
     *
     * @return the value of com_base_info.com_logo_archive
     *
     * @mbg.generated
     */
    public String getComLogoArchive() {
        return comLogoArchive;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_logo_archive
     *
     * @param comLogoArchive the value for com_base_info.com_logo_archive
     *
     * @mbg.generated
     */
    public void setComLogoArchive(String comLogoArchive) {
        this.comLogoArchive = comLogoArchive;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_video
     *
     * @return the value of com_base_info.com_video
     *
     * @mbg.generated
     */
    public String getComVideo() {
        return comVideo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_video
     *
     * @param comVideo the value for com_base_info.com_video
     *
     * @mbg.generated
     */
    public void setComVideo(String comVideo) {
        this.comVideo = comVideo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.horse_club
     *
     * @return the value of com_base_info.horse_club
     *
     * @mbg.generated
     */
    public Byte getHorseClub() {
        return horseClub;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.horse_club
     *
     * @param horseClub the value for com_base_info.horse_club
     *
     * @mbg.generated
     */
    public void setHorseClub(Byte horseClub) {
        this.horseClub = horseClub;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.unicorn_club
     *
     * @return the value of com_base_info.unicorn_club
     *
     * @mbg.generated
     */
    public Byte getUnicornClub() {
        return unicornClub;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.unicorn_club
     *
     * @param unicornClub the value for com_base_info.unicorn_club
     *
     * @mbg.generated
     */
    public void setUnicornClub(Byte unicornClub) {
        this.unicornClub = unicornClub;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_des
     *
     * @return the value of com_base_info.com_des
     *
     * @mbg.generated
     */
    public String getComDes() {
        return comDes;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_des
     *
     * @param comDes the value for com_base_info.com_des
     *
     * @mbg.generated
     */
    public void setComDes(String comDes) {
        this.comDes = comDes;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_url
     *
     * @return the value of com_base_info.com_url
     *
     * @mbg.generated
     */
    public String getComUrl() {
        return comUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_url
     *
     * @param comUrl the value for com_base_info.com_url
     *
     * @mbg.generated
     */
    public void setComUrl(String comUrl) {
        this.comUrl = comUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_born_year
     *
     * @return the value of com_base_info.com_born_year
     *
     * @mbg.generated
     */
    public Integer getComBornYear() {
        return comBornYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_born_year
     *
     * @param comBornYear the value for com_base_info.com_born_year
     *
     * @mbg.generated
     */
    public void setComBornYear(Integer comBornYear) {
        this.comBornYear = comBornYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_location
     *
     * @return the value of com_base_info.com_location
     *
     * @mbg.generated
     */
    public String getComLocation() {
        return comLocation;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_location
     *
     * @param comLocation the value for com_base_info.com_location
     *
     * @mbg.generated
     */
    public void setComLocation(String comLocation) {
        this.comLocation = comLocation;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_born_month
     *
     * @return the value of com_base_info.com_born_month
     *
     * @mbg.generated
     */
    public Integer getComBornMonth() {
        return comBornMonth;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_born_month
     *
     * @param comBornMonth the value for com_base_info.com_born_month
     *
     * @mbg.generated
     */
    public void setComBornMonth(Integer comBornMonth) {
        this.comBornMonth = comBornMonth;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_prov
     *
     * @return the value of com_base_info.com_prov
     *
     * @mbg.generated
     */
    public String getComProv() {
        return comProv;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_prov
     *
     * @param comProv the value for com_base_info.com_prov
     *
     * @mbg.generated
     */
    public void setComProv(String comProv) {
        this.comProv = comProv;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_city
     *
     * @return the value of com_base_info.com_city
     *
     * @mbg.generated
     */
    public String getComCity() {
        return comCity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_city
     *
     * @param comCity the value for com_base_info.com_city
     *
     * @mbg.generated
     */
    public void setComCity(String comCity) {
        this.comCity = comCity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_status_name
     *
     * @return the value of com_base_info.com_status_name
     *
     * @mbg.generated
     */
    public String getComStatusName() {
        return comStatusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_status_name
     *
     * @param comStatusName the value for com_base_info.com_status_name
     *
     * @mbg.generated
     */
    public void setComStatusName(String comStatusName) {
        this.comStatusName = comStatusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_close_date
     *
     * @return the value of com_base_info.com_close_date
     *
     * @mbg.generated
     */
    public String getComCloseDate() {
        return comCloseDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_close_date
     *
     * @param comCloseDate the value for com_base_info.com_close_date
     *
     * @mbg.generated
     */
    public void setComCloseDate(String comCloseDate) {
        this.comCloseDate = comCloseDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_stage_name
     *
     * @return the value of com_base_info.com_stage_name
     *
     * @mbg.generated
     */
    public String getComStageName() {
        return comStageName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_stage_name
     *
     * @param comStageName the value for com_base_info.com_stage_name
     *
     * @mbg.generated
     */
    public void setComStageName(String comStageName) {
        this.comStageName = comStageName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_fund_needs_name
     *
     * @return the value of com_base_info.com_fund_needs_name
     *
     * @mbg.generated
     */
    public String getComFundNeedsName() {
        return comFundNeedsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_fund_needs_name
     *
     * @param comFundNeedsName the value for com_base_info.com_fund_needs_name
     *
     * @mbg.generated
     */
    public void setComFundNeedsName(String comFundNeedsName) {
        this.comFundNeedsName = comFundNeedsName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_scale_name
     *
     * @return the value of com_base_info.com_scale_name
     *
     * @mbg.generated
     */
    public String getComScaleName() {
        return comScaleName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_scale_name
     *
     * @param comScaleName the value for com_base_info.com_scale_name
     *
     * @mbg.generated
     */
    public void setComScaleName(String comScaleName) {
        this.comScaleName = comScaleName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_fund_status_name
     *
     * @return the value of com_base_info.com_fund_status_name
     *
     * @mbg.generated
     */
    public String getComFundStatusName() {
        return comFundStatusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_fund_status_name
     *
     * @param comFundStatusName the value for com_base_info.com_fund_status_name
     *
     * @mbg.generated
     */
    public void setComFundStatusName(String comFundStatusName) {
        this.comFundStatusName = comFundStatusName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_weibo_url
     *
     * @return the value of com_base_info.com_weibo_url
     *
     * @mbg.generated
     */
    public String getComWeiboUrl() {
        return comWeiboUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_weibo_url
     *
     * @param comWeiboUrl the value for com_base_info.com_weibo_url
     *
     * @mbg.generated
     */
    public void setComWeiboUrl(String comWeiboUrl) {
        this.comWeiboUrl = comWeiboUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_weixin_url
     *
     * @return the value of com_base_info.com_weixin_url
     *
     * @mbg.generated
     */
    public String getComWeixinUrl() {
        return comWeixinUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_weixin_url
     *
     * @param comWeixinUrl the value for com_base_info.com_weixin_url
     *
     * @mbg.generated
     */
    public void setComWeixinUrl(String comWeixinUrl) {
        this.comWeixinUrl = comWeixinUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_cont_tel
     *
     * @return the value of com_base_info.com_cont_tel
     *
     * @mbg.generated
     */
    public String getComContTel() {
        return comContTel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_cont_tel
     *
     * @param comContTel the value for com_base_info.com_cont_tel
     *
     * @mbg.generated
     */
    public void setComContTel(String comContTel) {
        this.comContTel = comContTel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_cont_email
     *
     * @return the value of com_base_info.com_cont_email
     *
     * @mbg.generated
     */
    public String getComContEmail() {
        return comContEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_cont_email
     *
     * @param comContEmail the value for com_base_info.com_cont_email
     *
     * @mbg.generated
     */
    public void setComContEmail(String comContEmail) {
        this.comContEmail = comContEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_cont_addr
     *
     * @return the value of com_base_info.com_cont_addr
     *
     * @mbg.generated
     */
    public String getComContAddr() {
        return comContAddr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_cont_addr
     *
     * @param comContAddr the value for com_base_info.com_cont_addr
     *
     * @mbg.generated
     */
    public void setComContAddr(String comContAddr) {
        this.comContAddr = comContAddr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.cat_id
     *
     * @return the value of com_base_info.cat_id
     *
     * @mbg.generated
     */
    public Integer getCatId() {
        return catId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.cat_id
     *
     * @param catId the value for com_base_info.cat_id
     *
     * @mbg.generated
     */
    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.cat_name
     *
     * @return the value of com_base_info.cat_name
     *
     * @mbg.generated
     */
    public String getCatName() {
        return catName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.cat_name
     *
     * @param catName the value for com_base_info.cat_name
     *
     * @mbg.generated
     */
    public void setCatName(String catName) {
        this.catName = catName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.sub_cat_id
     *
     * @return the value of com_base_info.sub_cat_id
     *
     * @mbg.generated
     */
    public Integer getSubCatId() {
        return subCatId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.sub_cat_id
     *
     * @param subCatId the value for com_base_info.sub_cat_id
     *
     * @mbg.generated
     */
    public void setSubCatId(Integer subCatId) {
        this.subCatId = subCatId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.sub_cat_name
     *
     * @return the value of com_base_info.sub_cat_name
     *
     * @mbg.generated
     */
    public String getSubCatName() {
        return subCatName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.sub_cat_name
     *
     * @param subCatName the value for com_base_info.sub_cat_name
     *
     * @mbg.generated
     */
    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_relative_invst
     *
     * @return the value of com_base_info.com_relative_invst
     *
     * @mbg.generated
     */
    public String getComRelativeInvst() {
        return comRelativeInvst;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_relative_invst
     *
     * @param comRelativeInvst the value for com_base_info.com_relative_invst
     *
     * @mbg.generated
     */
    public void setComRelativeInvst(String comRelativeInvst) {
        this.comRelativeInvst = comRelativeInvst;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.invse_total_money
     *
     * @return the value of com_base_info.invse_total_money
     *
     * @mbg.generated
     */
    public String getInvseTotalMoney() {
        return invseTotalMoney;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.invse_total_money
     *
     * @param invseTotalMoney the value for com_base_info.invse_total_money
     *
     * @mbg.generated
     */
    public void setInvseTotalMoney(String invseTotalMoney) {
        this.invseTotalMoney = invseTotalMoney;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.com_type
     *
     * @return the value of com_base_info.com_type
     *
     * @mbg.generated
     */
    public Byte getComType() {
        return comType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.com_type
     *
     * @param comType the value for com_base_info.com_type
     *
     * @mbg.generated
     */
    public void setComType(Byte comType) {
        this.comType = comType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.create_time
     *
     * @return the value of com_base_info.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.create_time
     *
     * @param createTime the value for com_base_info.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column com_base_info.update_time
     *
     * @return the value of com_base_info.update_time
     *
     * @mbg.generated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column com_base_info.update_time
     *
     * @param updateTime the value for com_base_info.update_time
     *
     * @mbg.generated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}