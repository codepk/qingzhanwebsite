package com.qz.cn.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.qz.cn.mapper")
public class Mybatis {
}
